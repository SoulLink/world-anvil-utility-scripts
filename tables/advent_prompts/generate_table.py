from typing import List


files = [
    'prompts-links.txt',
    'prompts-texts.txt',
    'prompts-templates.txt',
    'article-titles.txt',
    'article-cluster.txt',
    'article-descriptions.txt',
    'article-status.txt',
    'article-purpose.txt'
]

expanded_result = True
base_link_prompts = 'https://www.worldanvil.com/worldbuilding-prompts/'
base_world_link = '/w/WBtV/a/'


def read_file(file: str) -> List[str]:
    with open(file, 'r') as fp:
        text = fp.read().split('\n')
    return text


if __name__ == '__main__':

    data = list()
    for file in files:
        lines = read_file(file)
        data.append(lines)

    result = ''

    for index in range(0, 31):
        result += f'{index + 1}'
        result += f',{base_link_prompts}{data[0][index]}|{data[1][index]}|tab|no'
        result += f',{data[2][index]}'

        if expanded_result:
            title = data[3][index]
            slug = title.replace(" ", "-").replace("'", "-").lower()
            if title != '':
                result += f',{base_world_link}{slug}-{data[2][index]}|{title}|navigate'
            else:
                result += ','
            description = data[5][index]
            if description != '':
                result += f',{description}'
            else:
                result += ','
            cluster = data[4][index]
            if cluster != '':
                result += f',{cluster}'
            else:
                result += ','
            status = data[6][index]
            if status != '':
                result += f',{status}'
            else:
                result += ','
            purpose = data[7][index]
            if purpose != '':
                result += f',{purpose}'
            else:
                result += ','
        result += '\n'

    with open('result.csv', 'w') as fp:
        fp.write(result)