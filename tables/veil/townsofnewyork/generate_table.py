import csv


towns = list()


with open('list_of_towns_in_new_york.csv', 'r') as fp:
    reader = csv.reader(fp)
    for row in reader:
        county = row[3].replace(' County', '')
        town = list()
        town.append(row[1].lower().replace(' ', '-') + '-' + county.lower().replace(' ', '-')) # key
        town.append(row[1]) # title
        town.append('text') # type
        town.append(f"A town in [url:{row[2]}|tab]{county}[/url], New York. Source: [url:{row[0]}|tab]Wikidata[/url]") # value
        towns.append(town)

with open('town_variables.csv', 'w') as fp:
    writer = csv.writer(fp, dialect='unix')
    for town in towns:
        writer.writerow(town)