import csv


counties = list()


with open('counties_of_new_york.csv', 'r') as fp:
    reader = csv.reader(fp)
    for row in reader:
        county = list()
        name = row[1].replace(' County', '')
        county.append(name.lower().replace(' ', '-').replace('.', '')) # key
        county.append(name) # title
        county.append('text') # type
        value = f"A county in @[New York State](location:e6e5e63e-59ed-44e7-8252-e0406181d41c) since {row[2][0:4]}. "
        if row[3] != "":
            value = value + f"It is named after [url:{row[3]}|tab]{row[4]}[/url]. "
        value = value + f"Source: [url:{row[0]}|tab]Wikidata[/url]"
        county.append(value) # value
        counties.append(county)

with open('county_variables.csv', 'w') as fp:
    writer = csv.writer(fp, dialect='unix')
    for county in counties:
        writer.writerow(county)