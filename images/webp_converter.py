import logging
import os
import subprocess
import sys

from pywaclient.api import BoromirApiClient

import argparse

parser = argparse.ArgumentParser(description='Download images from world anvil world and transform them to WEBP. '
                                             'Requires cwebp to be installed on the system and the python library '
                                             'pywaclient.'
                                             '\n'
                                             'pip --install pywaclient')

parser.add_argument('-t', '--token', dest='token', required=True, help='The authentication token of the account.')
parser.add_argument('-k', '--key', dest='key', required=True, help='The application key for the World Anvil API.')
parser.add_argument('-w', '--world', dest='world', required=True,
                    help='A world identifier of the user. Downloads and transforms all images of this world.')
parser.add_argument('-p', '--path', dest='base_path', default='output',
                    help='Determines the folder that the files are stored in. By default it creates a'
                         ' output folder in the current working directory.')

args = parser.parse_args()

app_key = args.key
auth_token = args.token
world_id = args.world
base_path = args.base_path

logging.basicConfig(
    stream=sys.stdout,
    level=logging.INFO
)

client = BoromirApiClient(
    'ImageUtility',
    'https://www.worldanvil.com/author/SoulLink', 'v1', app_key, auth_token
)

if not os.path.exists(base_path):
    os.mkdir(base_path)

world_name = client.world.get(world_id, 1)['title']

if __name__ == '__main__':
    logging.info(f"Loading files from world {world_name}.")
    for image in client.world.images(world_id):
        if not image['url'].endswith('.webp'):
            file_path = os.path.join(base_path, image['title'])
            if not os.path.exists(file_path):
                with open(file_path, 'wb') as fp:
                    for chunk in client.image.get_binary(image['id']):
                        fp.write(chunk)
                logging.info(f'Downloaded {image["title"]} to {file_path}.')
    logging.info(f"Finished downloading all image files from {world_name}.")
    logging.info("Begin transforming images to WEBP.")
    count = 0
    for root, directories, files in os.walk(base_path):
        total_files = len(files)
        for file in files:
            count += 1
            path = os.path.join(root, file)
            output_file = f'{path}.webp'
            if not os.path.exists(output_file):
                subprocess.run(['cwebp', '-q', '80', '-quiet', '-m', '6', path, '-o', output_file])
                logging.info(f'Transformed file {path}.')
            else:
                logging.info(f'Ignored file {path}, because a WEBP version already exists.')
            logging.info(f"Transformed {count}/{total_files} images.")
    logging.info("Finished transforming images to WEBP.")
