import logging
import os
import subprocess
import argparse
import re

parser = argparse.ArgumentParser()
parser.add_argument('path', default='.', type=str,
                    help='Path to the folder where the untransformed images are.')

args = parser.parse_args()
base_path = args.path

logging.info("Begin transforming images to WEBP.")
count = 0
for root, directories, files in os.walk(base_path):
    total_files = len(files)
    for file in files:
        count += 1
        file_without_extension = re.sub(r'\.[a-z]{3}', '', file)
        path = os.path.join(root, file)
        output_file = f'{os.path.join(root, file_without_extension)}.webp'
        if not os.path.exists(output_file):
            subprocess.run(['cwebp', '-q', '80', '-quiet', '-m', '6', path, '-o', output_file])
            logging.info(f'Transformed file {path}.')
        else:
            logging.info(f'Ignored file {path}, because a WEBP version already exists.')
        logging.info(f"Transformed {count}/{total_files} images.")
logging.info("Finished transforming images to WEBP.")
