import csv
import os

from pywaclient.api import AragornApiClient
from pywaclient.models.article import Article

if __name__ == '__main__':
    client = AragornApiClient(
        'WorldEmberUtilityScript',
        'https://www.worldanvil.com/author/SoulLink', 'v1', os.environ['WA_APPLICATION_KEY'],
        os.environ['WA_AUTH_TOKEN']
    )

    with open('output.csv', 'w') as fp:
        writer = csv.writer(fp, dialect='unix')
        for item in client.world.articles(identifier='3c374ab2-fb57-4870-830e-01262ea8db73'):
            article = Article(client, item)
            writer.writerow(
                [
                    article.title,
                    article.is_draft,
                    article.state,
                    article.last_update
                ]
            )

