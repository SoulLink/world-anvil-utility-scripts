import logging
import math
import os
import sys

from pywaclient.api import BoromirApiClient

logging.basicConfig(
    stream=sys.stdout,
    level=logging.INFO
)

rpg_system_list_without_heroes = dict()
rpg_system_first_letters_without_heroes = set()

rpg_system_list_with_heroes = dict()
rpg_system_first_letters_with_heroes = set()

heading_template = "[h2]{letter}[/h2]\n[ul]\n"
url_template = "  [li][url:{url}|tab]{title}[/url]"

TEMPLATE_INTRODUCTION = """[quote]
World Anvil has many RPG systems available. This page lists all of them in alphabetical order. They
are separated into two categories: systems that have a character statblock template available and
systems that do not have a character statblock template available. The systems that have a character
statblock template available are also available in the [url:https://www.worldanvil.com/community/rpg]RPG section[/url] 
of the site. This first list are all the systems that have a character statblock template available.
[/quote]

"""

TEMPLATE_INTRODUCTION_SECOND_PART = """[h1]Systems without templates[/h1]
[quote]
The following systems can be selected on World Anvil, but do not have a character statblock template available yet. 
To create one of these templates follow the instructions in the guide below.
[/quote]

[concol][articleblock:4e15e490-2db7-4a9a-b0b1-f8c8d1065c07][/concol] /* Custom Statblock Template Guide */

"""


def create_section(introduction: str, letters: set, rpg_systems: dict, items_per_column: int):
    section = introduction
    section += '[row][col3]\n'
    processed_results = 0
    for letter in sorted(letters):
        print(f"Processing letter {letter}.")
        heading = heading_template.format(letter=letter)
        processed_results += 4
        for rpg_system in sorted(rpg_systems[letter], key=lambda x: x["title"]):
            url = url_template.format(**rpg_system)
            heading += url + "[/li]\n"
            processed_results += 1
            if processed_results > items_per_column:
                section += heading + "[/ul]\n"
                section += "[/col3][col3]\n"
                print(f"Processed {processed_results} rpg systems. Creating new column.")
                processed_results = 4
                heading = heading_template.format(letter=letter)
        heading += "[/ul]\n"
        section += heading
    section += "\n[/col3][/row]"
    return section


def process_rpg_systems():
    rpg_systems = client.rpg_system.list()
    total = len(rpg_systems)
    print(f"Found {total} rpg systems.")
    count = 1
    for s in rpg_systems:
        rpg_system = client.rpg_system.get(s["id"], 1)
        first_letter = rpg_system["title"][0].upper()
        content = {
            "id": rpg_system["id"],
            "title": rpg_system["title"],
            "url": f"https://www.worldanvil.com/community/rpg/system/{rpg_system['reference']}",
            'heroesEnabled': rpg_system['heroesEnabled'],
            'firstLetter': first_letter,
        }
        if rpg_system['heroesEnabled']:
            if first_letter not in rpg_system_list_with_heroes:
                rpg_system_list_with_heroes[first_letter] = list()
                rpg_system_list_with_heroes[first_letter].append(content)
            else:
                rpg_system_list_with_heroes[first_letter].append(content)
            rpg_system_first_letters_with_heroes.add(first_letter)
        else:
            if first_letter not in rpg_system_list_without_heroes:
                rpg_system_list_without_heroes[first_letter] = list()
                rpg_system_list_without_heroes[first_letter].append(content)
            else:
                rpg_system_list_without_heroes[first_letter].append(content)
            rpg_system_first_letters_without_heroes.add(first_letter)
        print(f"Processed {count} of {total} rpg systems.")
        count += 1


if __name__ == "__main__":
    client = BoromirApiClient(
        "WorldAnvil Codex",
        "https://www.worldanvil.com/author/SoulLink",
        "v1",
        os.environ["WA_APPLICATION_KEY"],
        os.environ["WA_AUTH_TOKEN"],
    )
    process_rpg_systems()
    print(f"Found {len(rpg_system_first_letters_with_heroes)} letters with heroes.")
    rpg_systems_with_heroes = sum([len(rpg_system_list_with_heroes[item]) for item in rpg_system_list_with_heroes])
    rpg_system_letters_with_heroes = len(rpg_system_first_letters_with_heroes) * 4 + 8
    print(f"Total rpg systems with heroes: {rpg_systems_with_heroes}")
    items_per_column_with_heroes = math.floor(
        (rpg_systems_with_heroes +   rpg_system_letters_with_heroes) / 3)
    print(f"Items per column with heroes: {items_per_column_with_heroes}")
    first_section = create_section(TEMPLATE_INTRODUCTION, rpg_system_first_letters_with_heroes,
                                   rpg_system_list_with_heroes, items_per_column_with_heroes)

    print(f"Found {len(rpg_system_first_letters_without_heroes)} letters without heroes.")
    rpg_systems_without_heroes = sum([len(rpg_system_list_without_heroes[item]) for item in rpg_system_list_without_heroes])
    print(f"Total rpg systems without heroes: {rpg_systems_without_heroes}")
    rpg_system_letters_without_heroes = len(rpg_system_first_letters_without_heroes) * 4 + 8
    items_per_column_without_heroes = math.floor(
        (rpg_systems_without_heroes + rpg_system_letters_without_heroes) / 3)
    print(f"Items per column without heroes: {items_per_column_without_heroes}")
    second_section = create_section(TEMPLATE_INTRODUCTION_SECOND_PART, rpg_system_first_letters_without_heroes,
                                    rpg_system_list_without_heroes, items_per_column_without_heroes)
    with open("rpg_system_overview.txt", "w") as fp:
        fp.write(first_section)
        fp.write(second_section)

    print("Finished processing all rpg systems.")
