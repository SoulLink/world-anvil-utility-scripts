import json
import os
from simple_elastic import ElasticIndex

index = ElasticIndex('contest-submissions-sc22-v1', url='localhost:9200')

ignore_properties_in_loop = [
    'id',  # ignored
    'title',
    'slug',  # ignored
    'state',
    'url',  # ignored
    'entityClass',
    'icon',
    'subscribergroups',  # ignored
    'position',  # ignored
    'isWip',
    'isDraft',
    'excerpt',
    'template',
    'customArticleTemplate',
    'wordcount',
    'creationDate',
    'updateDate',
    'publicationDate',
    'notificationDate',
    'likes',
    'views',
    'tags',
    'metadata_user',  # ignored
    'metadata_article',  # ignored
    'css_classes',  # ignored
    'css_styles',  # ignored
    'passcode',  # ignored
    'author',
    'category',
    'world',
    'cover',
    'content',
    'contentParsed',
    'portrait',
    'flag',
    'fullRender'
]

prompt_map = {
    '163': 'An ocean, desert, plain, pocket dimension, or other large expanse',
    '164': 'Α religion or organisation connected to a natural phenomenon',
    '165': 'Α species that survives in an unlikely place',
    '166': 'Α vehicle or type of vehicle used for long journeys',
    '167': 'Α settlement beside or in a great expanse',
    '168': 'Α material only harvestable from nature',
    '169': 'Α culture who lives by, near or within an ocean, desert or other expanse',
    '170': 'Α food that marks a rite of passage for a culture in your world',
    '171': 'A settlement that leads the known world in something',
    '172': 'A military conflict resolved through excellent leadership',
    '173': 'A building associated with governance, leadership or change',
    '174': 'An organization associated with governance, leadership or change',
    '175': 'A profession associated with leaders in your world',
    '176': 'A title that commands respect from those in the know',
    '177': 'A great leader of their people',
    '178': 'A tradition or ceremony which confers an honor on someone',
    '179': "A settlement that was lost or discovered.",
    '180': 'An astonishing natural wonder.',
    '181': 'A recently discovered, or rediscovered, species.',
    '182': 'A lost or discovered artifact of significance or power.',
    '183': 'A technology lost, forgotten or shrouded in mystery.',
    '184': 'A lost or discovered monument.',
    '185': 'An explorer, researcher or other character motivated by discovery.',
    '186': 'A travel log or other document associated with discovery.',
    '187': 'A species considered monstrous by some',
    '188': 'A myth or urban legend about a "monster"',
    '189': 'A tradition which keeps monsters or bad luck away',
    '190': 'A condition considered monstrous or "unlucky" by some',
    '191': 'An organization considered cruel or monstrous by some',
    '192': 'A person considered villainous or monstrous',
    '193': "An artifact that embodies a hideous or monstrous idea",
}


def check_for_data(doc, doc_prop_name, source, source_1, source_2, source_3):
    if source_1 in source and source[source_1] is not None:
        if source_2 in source[source_1] and source[source_1][source_2] is not None:
            if source_3 in source[source_1][source_2] and source[source_1][source_2][source_3] is not None:
                doc[doc_prop_name] = source[source_1][source_2][source_3]


def check_for_data_relations(doc, doc_prop_name, source, source_1, source_2, source_3, source_4):
    if source_1 in source and source[source_1] is not None:
        if source_2 in source[source_1] and source[source_1][source_2] is not None:
            if source_3 in source[source_1][source_2] and source[source_1][source_2][source_3] is not None:
                if source_4 in source[source_1][source_2][source_3] and source[source_1][source_2][source_3][source_4] is not None:
                    doc[doc_prop_name] = source[source_1][source_2][source_3][source_4]


if __name__ == '__main__':
    for directory in os.listdir('articles'):
        print("Processing", directory)
        for file in os.listdir(f'articles/{directory}'):
            # print("Processing ", file)
            path = f'articles/{directory}/{file}'
            with open(path, 'r') as fp:
                article = json.load(fp)
            document = dict()
            document['duel'] = directory
            document['prompt'] = prompt_map[directory]
            document['title'] = article['title']
            document['template'] = article['template']
            document['entityClass'] = article['entityClass']
            if 'customArticleTemplate' in article:
                document['custom_article_template'] = article['customArticleTemplate']['name']
            document['is_wip'] = article['isWip']
            document['is_draft'] = article['isDraft']
            document['state'] = article['state']
            document['icon'] = article['icon']
            document['excerpt'] = article['excerpt']
            document['likes'] = article['likes']
            document['views'] = article['views']
            document['wordcount'] = article['wordcount']
            document['creationDate'] = article['creationDate']['date'].replace(' ', 'T')
            document['updateDate'] = article['updateDate']['date'].replace(' ', 'T')
            document['publicationDate'] = article['publicationDate']['date'].replace(' ', 'T')
            if 'notificationDate' in article and article['notificationDate'] is not None:
                document['notificationDate'] = article['notificationDate']['date'].replace(' ', 'T')
            if 'tags' in article and article['tags'] is not None:
                document['tags'] = article['tags'].split(',')
            document['url'] = article['url']
            if 'category' in article and article['category'] is not None:
                document['category.title'] = article['category']['title']
                document['category.url'] = article['category']['url']
            if 'portrait' in article and article['portrait'] is not None:
                document['portrait'] = True
            else:
                document['portrait'] = False
            if 'flag' in article and article['flag'] is not None:
                document['flag'] = True
            else:
                document['flag'] = False
            document['world.title'] = article['world']['title']
            document['world.url'] = article['world']['url']
            document['author.username'] = article['author']['title']
            document['author.url'] = article['author']['url']
            if 'cover' in article and article['cover'] is not None:
                document['cover'] = True
            else:
                document['cover'] = False
            document['content.vignette'] = article['content']
            for key in article:
                if key not in ignore_properties_in_loop:
                    if key in ['sections']:
                        for subkey in article[key]:
                            document['section.' + subkey] = article[key][subkey]['content']
                    elif key in ['relations']:
                        for subkey in article[key]:
                            if article[key][subkey]['type'] == 'singular':
                                document[f'relation.{subkey}.title'] = article[key][subkey]['items']['title']
                                document[f'relation.{subkey}.type'] = article[key][subkey]['items']['type']
                            elif article[key][subkey]['type'] == 'collection':
                                document[f'relation.{subkey}.title'] = list()
                                document[f'relation.{subkey}.type'] = list()
                                if 'items' in article[key][subkey]:
                                    for c in article[key][subkey]['items']:
                                        document[f'relation.{subkey}.title'].append(c['title'])
                                        document[f'relation.{subkey}.type'].append(c['type'])
                            elif article[key][subkey]['type'] == 'article':
                                document[f'relation.{subkey}.title'] = article[key][subkey]['title']
                            elif article[key][subkey]['type'] == 'timeline':
                                document[f'relation.{subkey}.title'] = article[key][subkey]['title']
                                document[f'relation.{subkey}.type'] = article[key][subkey]['type']
                            else:
                                print("Relation Type:", article[key][subkey]['type'])
                    else:
                        print(key)
            index.index_into(document, article['id'])
