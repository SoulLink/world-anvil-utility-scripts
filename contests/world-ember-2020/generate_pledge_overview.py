import logging
import os
import sys
from datetime import datetime
from typing import Any, Dict, List

from fold_to_ascii import fold

from pywaclient.api import AragornApiClient
from pywaclient.models.article import Article
from pywaclient.models.world import World

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
heading = '[h1]{heading}[/h1]'
h2heading = '[h2]{heading}[/h2]'

language_map = {
    'en': 'English',
    'de': 'German',
    'es': 'Spanish'
}

article_block = '[articleblock:{id}]'

rows = """
[row]
[col]
{col1}
[/col]
[col]
{col2}
[/col]
[/row]
"""


def add_item(target_dict, text, added_item):
    if isinstance(added_item, str):
        added_item = article_block.format(id=added_item)
    if text in target_dict:
        target_dict[text].append(added_item)
    else:
        target_dict[text] = [added_item]


def generate_alphabetical_article(source_dict: Dict[str, Any]) -> str:
    content = ''
    keys = sorted(source_dict.keys(), key=lambda x: x.upper())
    by_alphabet = dict()
    for key in keys:
        ascii_key = fold(key)
        for a in ascii_key:
            if a.isascii():
                new_item = {
                    'heading': key,
                    'block': source_dict[key][0]
                }
                add_item(by_alphabet, a.upper(), new_item)
                break

    for key in by_alphabet:
        sorted_sublist = sorted(by_alphabet[key], key=lambda x: x['heading'])
        content += heading.format(heading=key)
        content += '\n'
        for s in sorted_sublist:
            content += h2heading.format(heading=s['heading'])
            content += '\n'
            content += s['block']
            content += '\n'
    return content


def generate_article(source_dict: Dict[str, Any]) -> str:
    content = ''

    sorted_keys = sorted(source_dict)

    for key in sorted_keys:
        content += heading.format(heading=key)
        content += '\n'
        col1 = list()
        col2 = list()
        count = 1
        for block in source_dict[key]:
            if count % 2 == 1:
                col1.append(block)
            elif count % 2 == 0:
                col2.append(block)
            count += 1

        content += rows.format(col1="\n".join(col1), col2="\n".join(col2))
    return content


def check_unique(lines: List[str]) -> bool:
    values = set(lines)
    return len(lines) == len(values)


if __name__ == '__main__':
    logging.info(f"Start process {datetime.now().isoformat()}")
    client = AragornApiClient(
        'WorldEmberUtilityScript',
        'https://www.worldanvil.com/author/SoulLink', 'v1', os.environ['WA_APPLICATION_KEY'], os.environ['WA_AUTH_TOKEN']
    )
    logging.info(f"Connected to API {datetime.now().isoformat()}")
    with open('pledge_articles.txt', 'r') as fp:
        lines = fp.readlines()

    logging.info(f"Checking unqiue {datetime.now().isoformat()}")
    if not check_unique(lines):
        print("Not all articles are unique.")
        exit(1)

    articles_by_language = dict()
    articles_by_genre = dict()
    articles_by_title = dict()
    articles_by_author = dict()
    logging.info(f"Checked lines {datetime.now().isoformat()}")
    for line in lines:
        try:
            line = line.strip('\n[]').replace('articleblock:', '')
            logging.info(f"Loaded pledge {line}: {datetime.now().isoformat()}")
            article = Article(client, client.article.get(line))
            world = article.world_id
            world = World(client, client.world.get(world))
            add_item(articles_by_title, world.name, article.id)
            author = article.author
            add_item(articles_by_author, author.username, article.id)
            add_item(articles_by_language, language_map[world.locale], article.id)
            genres = [x for x in world.genres]
            if len(genres) == 0:
                add_item(articles_by_genre, 'No Genre', article.id)
            else:
                for item in genres:
                    add_item(articles_by_genre, item.name, article.id)
        except TypeError:
            print(line)
            pass

    logging.info(f"Loaded data: {datetime.now().isoformat()}")
    content_lang = generate_article(articles_by_language)
    content_genre = generate_article(articles_by_genre)
    content_title = generate_alphabetical_article(articles_by_title)
    content_author = generate_alphabetical_article(articles_by_author)

    with open('pledges_by_language.txt', 'w') as pl:
        pl.write(content_lang)
    with open('pledges_by_genre.txt', 'w') as pg:
        pg.write(content_genre)

    with open('pledges_by_title.txt', 'w') as pl:
        pl.write(content_title)
    with open('pledges_by_author.txt', 'w') as pg:
        pg.write(content_author)
    logging.info(f"End process {datetime.now().isoformat()}")
