import csv
import os
import re

import requests
from pywaclient.api import AragornApiClient
from pywaclient.exceptions import AccessForbidden, InternalServerException
from pywaclient.models.article import Article


def get_identifiers():
    identifiers = list()
    response = requests.get(challenge_page)
    if response.ok:
        page = response.text
        matches = re.findall(regex, page)
        for match in matches:
            item = dict()
            article_url = url.format(match[1], match[2])
            article_id = match[0]
            item['id'] = article_id
            item['url'] = article_url
            identifiers.append(item)
    return identifiers


client = AragornApiClient(
    'WorldEmberUtilityScript',
    'https://www.worldanvil.com/author/SoulLink', 'v1', os.environ['WA_APPLICATION_KEY'], os.environ['WA_AUTH_TOKEN']
)

url = 'https://www.worldanvil.com/w/{}/a/{}'
challenge_page = 'https://www.worldanvil.com/community/challenge/rivers_waterways/homepage'
regex = '<a data-article-id="(.*)" target="_blank" href="/w/(.*)/a/(.*)">(.*)</a>'

if __name__ == '__main__':

    articles = get_identifiers()
    length = len(articles)
    count = 0
    with open('output.csv', 'w') as fp:
        writer = csv.writer(fp)
        writer.writerow(
            [
                'URL',
                'Title',
                'Author',
                'Template',
                'World',
                'Genre',
                'Language',
                'Word Count',
                'Last Update',
                'ID (sort)'
        ])
        for article in articles:
            try:
                article = Article(client, client.article.get(article['id']))
                try:
                    world = article.world
                except InternalServerException:
                    continue
                writer.writerow(
                    [
                        article.url,
                        article.title,
                        article.author.username,
                        article.template,
                        world.name,
                        "; ".join([x.name for x in world.genres]),
                        world.locale,
                        article.wordcount,
                        article.last_update,
                        article.id]
                )
                count += 1
                print(f"Progress: {count}/{length}.")
            except AccessForbidden:
                count += 1
                print(f"Article is private or draft: {article['url']}.")
                print(f"Progress: {count}/{length}.")
