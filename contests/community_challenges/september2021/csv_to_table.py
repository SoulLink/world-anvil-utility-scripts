import csv

if __name__ == '__main__':
    text = "[container: spoiler-table][spoiler][table]{}[/table]|Table - Click to Expand[/spoiler][/container]"
    header = "[tr][th]{}[/th][th]{}[/th][/tr]\n"
    row = "[tr][td]{}[/td][td][center]{}[/center][/td][/tr]\n"
    result = ""
    with open('source.csv', 'r') as source:
        reader = csv.reader(source)
        count = 1
        for line in reader:
            if count == 1:
                h = header.format(line[0], line[1])
                result += h
                count += 1
            else:
                r = row.format(line[0], line[1])
                result += r

    with open('output.bbcode', 'w') as bbcode:
        bbcode.write(text.format(result))