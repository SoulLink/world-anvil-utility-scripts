### Shipwright Challenge September 2021

[Challenge Page](https://www.worldanvil.com/community/challenge/shipwright_challenge/homepage)

#### Core Stats

- Total Submissions:
- Total Valid Submissions: 123
- Wrong Template: 2
- Private Articles: 2
- Draft Articles: 6
- Missing Articles: 1


#### Word Counts

- Median: 1'093
- Average: 1'317.014
- Minimum: 82
- Maximum: 3'268
- Sum: 161'996