import json
import os
from simple_elastic import ElasticIndex

index = ElasticIndex('september-2021', url='localhost:9200')

property_list = [
    'title',
    'template',
    'is_wip',
    'is_draft',
    'state',
    'wordcount',
    'creation_date',
    'update_date',
    'publication_date',
    'notification_date',
    'tags',
    'url',
    'category',
    'world',
    'author',
    'cover',
    'content',
    'portrait',

    # ignored fields:
    'full_render',
    'id',
    'metadata_user',
    'metadata_article',
    'css_classes',
    'css_styles',
    'passcode',
    'content_parsed',
]


def check_for_data(doc, doc_prop_name, source, source_1, source_2, source_3):
    if source_1 in source and source[source_1] is not None:
        if source_2 in source[source_1] and source[source_1][source_2] is not None:
            if source_3 in source[source_1][source_2] and source[source_1][source_2][source_3] is not None:
                doc[doc_prop_name] = source[source_1][source_2][source_3]


def check_for_data_relations(doc, doc_prop_name, source, source_1, source_2, source_3, source_4):
    if source_1 in source and source[source_1] is not None:
        if source_2 in source[source_1] and source[source_1][source_2] is not None:
            if source_3 in source[source_1][source_2] and source[source_1][source_2][source_3] is not None:
                if source_4 in source[source_1][source_2][source_3] and source[source_1][source_2][source_3][source_4] is not None:
                    doc[doc_prop_name] = source[source_1][source_2][source_3][source_4]


if __name__ == '__main__':
    for file in os.listdir('articles'):
        print("Processing ", file)
        path = f'articles/{file}'
        with open(path, 'r') as fp:
            article = json.load(fp)
        document = dict()
        document['title'] = article['title']
        document['template'] = article['template']
        document['is_wip'] = article['is_wip']
        document['is_draft'] = article['is_draft']
        document['state'] = article['state']
        document['wordcount'] = article['wordcount']
        document['creation_date'] = article['creation_date']['date'].replace(' ', 'T')
        document['update_date'] = article['update_date']['date'].replace(' ', 'T')
        if 'publication_date' in article and article['publication_date'] is not None:
            if 'date' in article['publication_date']:
                document['publication_date'] = article['publication_date']['date'].replace(' ', 'T')
            else:
                print(article['url'])
        if 'notification_date' in article and article['notification_date'] is not None:
            document['notification_date'] = article['notification_date']['date'].replace(' ', 'T')
        if 'tags' in article and article['tags'] is not None:
            document['tags'] = article['tags'].split(',')
            if document['tags'][0] == '':
                del document['tags']
        document['url'] = article['url']
        if 'category' in article and article['category'] is not None:
            document['category.title'] = article['category']['title']
            document['category.url'] = article['category']['url']
        if 'portrait' in article and article['portrait'] is not None:
            document['portrait'] = True
        else:
            document['portrait'] = False
        document['world.title'] = article['world']['title']
        document['world.url'] = article['world']['url']
        document['author.username'] = article['author']['username']
        document['author.url'] = article['author']['url']
        if 'cover' in article and article['cover'] is not None:
            document['cover'] = True
        else:
            document['cover'] = False
        document['content.vignette'] = article['content']
        for key in article:
            if key not in property_list:
                if key in ['sections']:
                    for subkey in article[key]:
                        document['section.' + subkey] = article[key][subkey]['content']
                elif key in ['relations']:
                    for subkey in article[key]:
                        if article[key][subkey]['type'] == 'singular':
                            document[f'relation.{subkey}.title'] = article[key][subkey]['items']['title']
                            document[f'relation.{subkey}.type'] = article[key][subkey]['items']['type']
                        elif article[key][subkey]['type'] == 'collection':
                            document[f'relation.{subkey}.title'] = list()
                            document[f'relation.{subkey}.type'] = list()
                            for c in article[key][subkey]['items']:
                                document[f'relation.{subkey}.title'].append(c['title'])
                                document[f'relation.{subkey}.type'].append(c['type'])
                        elif article[key][subkey]['type'] == 'article':
                            document[f'relation.{subkey}.title'] = article[key][subkey]['title']
                        elif article[key][subkey]['type'] == 'timeline':
                            document[f'relation.{subkey}.title'] = article[key][subkey]['title']
                            document[f'relation.{subkey}.type'] = article[key][subkey]['type']
                        else:
                            print("Relation Type:", article[key][subkey]['type'])
                else:
                    print(key)
        index.index_into(document, article['id'])
