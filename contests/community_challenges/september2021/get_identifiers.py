import json
import re

import requests

url = 'https://www.worldanvil.com/w/{}/a/{}'
homepage = 'https://www.worldanvil.com/community/challenge/shipwright_challenge/homepage'
regex = '<a data-article-id="(.*)" target="_blank" href="/w/(.*)/a/(.*)">(.*)</a>'

if __name__ == '__main__':
    identifiers = list()
    response = requests.get(homepage)
    if response.ok:
        page = response.text
        matches = re.findall(regex, page)
        for match in matches:
            item = dict()
            article_url = url.format(match[1], match[2])
            article_id = match[0]
            item['id'] = article_id
            item['url'] = article_url
            item['title'] = match[2]
            identifiers.append(item)
    with open('articles.json', 'w') as fp:
        json.dump(identifiers, fp, indent='    ', ensure_ascii=False)
