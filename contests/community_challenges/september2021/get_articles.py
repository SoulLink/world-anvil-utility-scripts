import json
import os

from pywaclient.api import AragornApiClient
from pywaclient.exceptions import AccessForbidden

client = AragornApiClient(
    'WorldEmberUtilityScript',
    'https://www.worldanvil.com/author/SoulLink', 'v1', os.environ['WA_APPLICATION_KEY'], os.environ['WA_AUTH_TOKEN']
)

if __name__ == '__main__':
    with open('articles.json', 'r') as fp:
        articles = json.load(fp)

        length = len(articles)
        print(f"Downloading {length} articles.")
        count = 0
        for article in articles:
            try:
                item = client.article.get(article['id'])
                with open(f'articles/{article["id"]}.json', 'w') as out:
                    json.dump(item, out)
                count += 1
                print(f"Progress: {count}/{length}.")
            except AccessForbidden:
                count += 1
                print(f"Could not process: {article['url']}.")
