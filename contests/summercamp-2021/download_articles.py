import json
import os

from pywaclient.api import AragornApiClient

client = AragornApiClient(
    'WorldEmberUtilityScript',
    'https://www.worldanvil.com/author/SoulLink', 'v1', os.environ['WA_APPLICATION_KEY'], os.environ['WA_AUTH_TOKEN']
)


if __name__ == '__main__':

    for item in range(109, 140):
        if not os.path.exists(f'articles/{item}'):
            os.mkdir(f'articles/{item}')
        with open('article_not_found.csv', 'w') as x:
            with open(f'ids/{item}.csv', 'r') as file:
                print(item)
                for line in file.readlines():
                    line_split = line.strip().split(',')
                    article = client.article.get(line_split[0])
                    if article is not None:
                        with open(f'articles/{item}/{article["id"]}.json', 'w') as fp:
                            json.dump(article, fp)
                    else:
                        x.write(line + '\n')