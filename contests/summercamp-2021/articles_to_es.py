import json
import os
from simple_elastic import ElasticIndex

index = ElasticIndex('contest-submissions-v1', url='localhost:9200')

property_list = [
    'title',
    'template',
    'is_wip',
    'is_draft',
    'state',
    'wordcount',
    'creation_date',
    'update_date',
    'publication_date',
    'notification_date',
    'tags',
    'url',
    'category',
    'world',
    'author',
    'cover',
    'content',
    'portrait',

    # ignored fields:
    'full_render',
    'id',
    'metadata_user',
    'metadata_article',
    'css_classes',
    'css_styles',
    'passcode',
    'content_parsed',
]

prompt_map = {
    '109': 'A building associated with healing the sick',
    '110': 'A medical condition which is feared by some',
    '111': 'A new medical cure, treatment or breakthrough',
    '112': 'A species of working animal',
    '113': 'An ethnicity whose cultural exports are highly sought after',
    '114': 'An extraordinary writing tool (or type of writing tool)',
    '115': 'An area or geographical landmark wrapped in myth, legend or superstition',
    '116': 'A myth about a mountain, lake, cave or other landmark',
    '117': 'A myth or fairytale about a prophecy, oracle, fortune-telling or other revelation',
    '118': 'A festival associated with a celestial body',
    '119': 'An inhospitable region or geographical landmark',
    '120': 'A plant or animal that lives in an inhospitable region',
    '121': 'An ethnicity surviving in an inhospitable region',
    '122': 'An old or ancient organization which still casts a shadow',
    '123': 'A lost or ancient language',
    '124': 'An ancient and/or powerful artifact',
    '125': "A religious order founded or based in your world's history",
    '126': 'A coming of age ceremony',
    '127': 'A famous letter or message',
    '128': 'An often undervalued but vital profession',
    '129': 'A building associated with crime or justice',
    '130': 'A renowned pirate, highwayman or other criminal',
    '131': 'A unit or squad who guards an important person',
    '132': 'A settlement known as a pleasure town or for louche behaviour',
    '133': 'A brave hero, who hides a secret past',
    '134': 'A Romancer, Paramour, or other amorous individual',
    '135': 'A large business, corporation, or trade guild',
    '136': 'A vital trade resource that supports a settlement or region',
    '137': 'An industrial, mining, logging or other production settlement',
    '138': 'A title which cannot be bought, only earned',
    '139': "A bloody coup, rebellion or uprising in your world's history.",
}


def check_for_data(doc, doc_prop_name, source, source_1, source_2, source_3):
    if source_1 in source and source[source_1] is not None:
        if source_2 in source[source_1] and source[source_1][source_2] is not None:
            if source_3 in source[source_1][source_2] and source[source_1][source_2][source_3] is not None:
                doc[doc_prop_name] = source[source_1][source_2][source_3]


def check_for_data_relations(doc, doc_prop_name, source, source_1, source_2, source_3, source_4):
    if source_1 in source and source[source_1] is not None:
        if source_2 in source[source_1] and source[source_1][source_2] is not None:
            if source_3 in source[source_1][source_2] and source[source_1][source_2][source_3] is not None:
                if source_4 in source[source_1][source_2][source_3] and source[source_1][source_2][source_3][source_4] is not None:
                    doc[doc_prop_name] = source[source_1][source_2][source_3][source_4]


if __name__ == '__main__':
    for directory in os.listdir('articles'):
        print("Processing", directory)
        for file in os.listdir(f'articles/{directory}'):
            print("Processing ", file)
            path = f'articles/{directory}/{file}'
            with open(path, 'r') as fp:
                article = json.load(fp)
            document = dict()
            document['duel'] = directory
            document['prompt'] = prompt_map[directory]
            document['title'] = article['title']
            document['template'] = article['template']
            document['is_wip'] = article['is_wip']
            document['is_draft'] = article['is_draft']
            document['state'] = article['state']
            document['wordcount'] = article['wordcount']
            document['creation_date'] = article['creation_date']['date'].replace(' ', 'T')
            document['update_date'] = article['update_date']['date'].replace(' ', 'T')
            document['publication_date'] = article['publication_date']['date'].replace(' ', 'T')
            if 'notification_date' in article and article['notification_date'] is not None:
                document['notification_date'] = article['notification_date']['date'].replace(' ', 'T')
            if 'tags' in article and article['tags'] is not None:
                document['tags'] = article['tags'].split(',')
            document['url'] = article['url']
            if 'category' in article and article['category'] is not None:
                document['category.title'] = article['category']['title']
                document['category.url'] = article['category']['url']
            if 'portrait' in article and article['portrait'] is not None:
                document['portrait'] = True
            else:
                document['portrait'] = False
            document['world.title'] = article['world']['title']
            document['world.url'] = article['world']['url']
            document['author.username'] = article['author']['username']
            document['author.url'] = article['author']['url']
            if 'cover' in article and article['cover'] is not None:
                document['cover'] = True
            else:
                document['cover'] = False
            document['content.vignette'] = article['content']
            for key in article:
                if key not in property_list:
                    if key in ['sections']:
                        for subkey in article[key]:
                            document['section.' + subkey] = article[key][subkey]['content']
                    elif key in ['relations']:
                        for subkey in article[key]:
                            if article[key][subkey]['type'] == 'singular':
                                document[f'relation.{subkey}.title'] = article[key][subkey]['items']['title']
                                document[f'relation.{subkey}.type'] = article[key][subkey]['items']['type']
                            elif article[key][subkey]['type'] == 'collection':
                                document[f'relation.{subkey}.title'] = list()
                                document[f'relation.{subkey}.type'] = list()
                                for c in article[key][subkey]['items']:
                                    document[f'relation.{subkey}.title'].append(c['title'])
                                    document[f'relation.{subkey}.type'].append(c['type'])
                            elif article[key][subkey]['type'] == 'article':
                                document[f'relation.{subkey}.title'] = article[key][subkey]['title']
                            elif article[key][subkey]['type'] == 'timeline':
                                document[f'relation.{subkey}.title'] = article[key][subkey]['title']
                                document[f'relation.{subkey}.type'] = article[key][subkey]['type']
                            else:
                                print("Relation Type:", article[key][subkey]['type'])
                    else:
                        print(key)
            index.index_into(document, article['id'])
