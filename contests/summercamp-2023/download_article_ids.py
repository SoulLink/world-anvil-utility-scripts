import requests
import re

if __name__ == '__main__':
    base = 'https://www.worldanvil.com'
    sc_challenge_page = base + '/community/challenge/summercamp-2023/duel/{}'
    with open(f'failures.csv', 'w') as fail:
        for i in range(224, 266):
            with open(f'ids/{i}.csv', 'w') as fp:
                page = requests.get(sc_challenge_page.format(i))
                content = page.text
                matches = re.findall('<a class="btn btn-primary" href="(.*)" target="">Read the article</a>', content)
                for match in matches:
                    article_url = base + match
                    article_page = requests.get(article_url)
                    if article_page.ok:
                        article_content = article_page.text
                        result = re.search('page-article page-article-main template-.+ article-([a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12})',
                                           article_content)
                        print(result)
                        if result is not None:
                            print(f'{result.group(1)},{article_url}')
                            fp.write(f'{result.group(1)},{article_url}\n')
                        else:
                            fail.write(f'MULTIPASS,{article_url}\n')
                    else:
                        fail.write(f'INVALID,{article_url}\n')
