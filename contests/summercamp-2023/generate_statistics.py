import json
import math
import os
from statistics import median

prompt_map = {
    '224': 'A powerful organization in your world',
    '225': 'A seat of power (of any kind!)',
    '226': 'A resource that provides fuel or power',
    '227': 'An animal associated with, or symbolizing, power',
    '228': 'A culture that has suffered under the rule of a stronger nation',
    '229': 'A conflict between two unequal powers in your world',
    '230': 'The title & responsibilities of an important person in your world',
    '231': 'A destructive natural or supernatural event',
    '232': 'A species known for its mischievous personality',
    '233': 'A popular summer tradition that involves art and creativity',
    '234': 'An unclaimed, unregulated, or lawless region in your setting',
    '235': 'A settlement at the limits of the "known" or "civilized" world',
    '236': 'A job that takes its practitioners to remote or faraway places',
    '237': 'An animal found in a non-populated area',
    '238': 'A useful plant found in a wild area of your world',
    '239': 'A material or natural resource that comes from a dangerous location',
    '240': "A character driven by wanderlust or the desire to explore",
    '241': 'A cuisine from a sparse, barren or remote region in your world',
    '242': 'An iconic building or landmark representing a location',
    '243': 'A letter sent in secret by a well-known person in your world',
    '244': 'A tradition or behavior considered old fashioned',
    '245': 'An item of great cultural or religious significance to a people in your world',
    '246': 'An ancient city that is still inhabited today',
    '247': 'A historical figure still venerated today, and why',
    '248': 'A profession that has been rendered obsolete',
    '249': "A children's tale or song based on a real event",
    '250': 'A species now considered extinct',
    '251': 'A historical culture whose influence is still felt today',
    '252': 'A ceremony that represents a transition or transfer',
    '253': 'A rare natural phenomenon that most people look forward to',
    '254': "A method used to carry goods over long distances",
    '255': 'A system to send messages between distant places',
    '256': 'A form of silent communication',
    '257': 'An organization for which recruiting or proselytizing is important',
    '258': 'A species with an unusual form of communication',
    '259': 'A character who excels in manipulating others',
    '260': 'An important public announcement that one person addressed to many',
    '261': 'A building or landmark used for, or associated with, communication',
    '262': 'A character who prefers to lurk in the shadows',
    '263': 'A "negative" condition that has hidden advantages',
    '264': 'An organisation dedicated to keeping a major secret from the public',
    '265': 'A myth or truth about the meaning of your universe',
}


output = dict()


if __name__ == '__main__':

    total_submissions = 0
    total_words = 0
    pronounciations_filled_in = 0
    all_authors = set()
    highest_wordcount_total = dict()
    word_counts_per_submission_total = list()
    total_views = 0
    total_comments = 0
    total_likes = 0
    articles_done = 0
    total_excerpts = 0
    mottos = list()
    demonyms = list()
    total_portrait = 0
    quotes = list()
    genderidentity = list()
    circumstancesDeath = list()
    nickname = list()
    types = dict()
    for root, directories, files in os.walk('articles2'):
        if len(files) > 0:
            print(root)
            highest_wordcount = dict()
            prompt = root.split('/')[-1]
            output[prompt] = dict()
            output[prompt]['prompt'] = prompt_map[prompt]
            number_of_files = len(files)
            output[prompt]['totalSubmissions'] = number_of_files
            total_submissions += number_of_files
            total_words_in_prompt = 0
            word_counts_per_submission = list()
            views = 0
            comments = 0
            likes = 0
            articles_done_per_prompt = 0
            template = ''
            for file in files:
                with open(f'{root}/{file}', 'r') as fp:
                    article = json.load(fp)
                    total_words_in_prompt += article['wordcount']
                    total_words += article['wordcount']
                    word_counts_per_submission.append(article['wordcount'])
                    previous_count = highest_wordcount.get('wordcount', 0)
                    highest_wordcount['wordcount'] = max(highest_wordcount.get('wordcount', 0), article['wordcount'])
                    if previous_count < highest_wordcount.get('wordcount', 0):
                        highest_wordcount['article'] = dict()
                        highest_wordcount['article']['title'] = article['title']
                        highest_wordcount['article']['url'] = article['url']
                        highest_wordcount['article']['author'] = article['author']['title']
                        highest_wordcount['prompt'] = prompt_map[prompt]
                        highest_wordcount['prompt_id'] = prompt
                    all_authors.add(article['author']['title'])
                    if article['pronunciation'] != '' and article['pronunciation'] is not None:
                        pronounciations_filled_in += 1
                    views += article['views']
                    template = article['templateType']
                    if 'comments' in article:
                        comments += len(article['comments'])
                    if 'fans' in article:
                        likes += len(article['fans'])
                    if not article['isWip']:
                        articles_done_per_prompt += 1
                    if article['excerpt'] != '' and article['excerpt'] is not None:
                        total_excerpts += 1
                    if 'motto' in article and article['motto'] != '' and article['motto'] is not None:
                        mottos.append(article['motto'])
                    if 'demonym' in article and article['demonym'] != '' and article['demonym'] is not None:
                        demonyms.append(article['demonym'])
                    if 'portrait' in article and article['portrait'] is not None:
                        total_portrait += 1
                    if 'quote' in article and article['quote'] != '' and article['quote'] is not None:
                        quotes.append(article['quote'])
                    if 'genderidentity' in article and article['genderidentity'] != '' and article['genderidentity'] is not None:
                        genderidentity.append(article['genderidentity'])
                    if 'circumstancesDeath' in article and article['circumstancesDeath'] != '' and article['circumstancesDeath'] is not None:
                        circumstancesDeath.append(article['circumstancesDeath'])
                    if 'nickname' in article and article['nickname'] != '' and article['nickname'] is not None:
                        nickname.append(article['nickname'])
                    if 'type' in article and article['type'] != '' and article['type'] is not None:
                        if template not in types:
                            types[template] = list()
                        if isinstance(article['type'], str):
                            types[template].append(article['type'])
                        elif isinstance(article['type'], dict):
                            types[template].append(article['type']['title'])
                        else:
                            print(article['type'])
            output[prompt]['totalWords'] = total_words_in_prompt
            output[prompt]['averageWordsPerSubmission'] = math.floor(total_words_in_prompt / number_of_files)
            output[prompt]['highestWordcount'] = highest_wordcount
            highest_wordcount_total = highest_wordcount if highest_wordcount.get('wordcount', 0) > highest_wordcount_total.get('wordcount', 0) else highest_wordcount_total

            output[prompt]['medianWordsPerSubmission'] = median(word_counts_per_submission)
            word_counts_per_submission_total.extend(word_counts_per_submission)
            output[prompt]['views'] = views
            output[prompt]['comments'] = comments
            output[prompt]['likes'] = likes
            total_views += views
            total_comments += comments
            total_likes += likes
            output[prompt]['numberOfArticlesSetAsDone'] = articles_done_per_prompt
            output[prompt]['numberOfArticlesSetAsDonePercentage'] = articles_done_per_prompt / number_of_files * 100
            articles_done += articles_done_per_prompt
            output[prompt]['template'] = template

    output['totalSubmissions'] = total_submissions
    output['totalWords'] = total_words
    output['pronounciationsFilledIn'] = pronounciations_filled_in
    output['pronounciationsFilledInPercentage'] = pronounciations_filled_in / total_submissions * 100
    output['averageWordsPerSubmission'] = math.floor(total_words / total_submissions)
    output['medianWordsPerSubmission'] = median(word_counts_per_submission_total)
    output['highestWordcount'] = highest_wordcount_total
    output['totalAuthors'] = len(all_authors)
    output['authors'] = list(sorted(all_authors))
    output['totalViews'] = total_views
    output['totalComments'] = total_comments
    output['totalLikes'] = total_likes
    output['numberOfArticlesSetAsDone'] = articles_done
    output['numberOfArticlesSetAsDonePercentage'] = articles_done / total_submissions * 100
    output['totalExcerpts'] = total_excerpts
    output['totalExcerptsPercentage'] = total_excerpts / total_submissions * 100
    output['totalPortrait'] = total_portrait
    output['mottos'] = mottos
    output['mottosCount'] = len(mottos)
    output['demonyms'] = demonyms
    output['demonymsCount'] = len(demonyms)
    output['quotes'] = quotes
    output['quotesCount'] = len(quotes)
    output['genderidentity'] = genderidentity
    output['genderidentityCount'] = len(genderidentity)
    output['circumstancesDeath'] = circumstancesDeath
    output['circumstancesDeathCount'] = len(circumstancesDeath)
    output['nickname'] = nickname
    output['nicknameCount'] = len(nickname)
    output['types'] = types



with open('statistics.json', 'w') as fp:
    json.dump(output, fp, indent=4, sort_keys=True, ensure_ascii=False)
