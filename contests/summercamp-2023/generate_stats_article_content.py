import json

if __name__ == '__main__':

    with open('statistics.json', 'r') as fp:
        statistics = json.load(fp)


    with open('mottos-spoiler.txt', 'w') as out:
        out.write(f'[spoiler][table]\n')
        for item in statistics['mottos']:
            out.write(f'[tr][td]{item}[/td][/tr]\n')
        out.write(f'[/table]|Mottos ({statistics["mottosCount"]})[/spoiler]\n')

    with open('demonyms-spoiler.txt', 'w') as out:
        out.write(f'[spoiler][table]\n')
        for item in statistics['demonyms']:
            out.write(f'[tr][td]{item}[/td][/tr]\n'.replace('|', ' '))
        out.write(f'[/table]|Demonyms ({statistics["demonymsCount"]})[/spoiler]\n')

    with open('genderidentity-spoiler.txt', 'w') as out:
        out.write(f'[spoiler][table]\n')
        for item in statistics['genderidentity']:
            out.write(f'[tr][td]{item}[/td][/tr]\n'.replace('|', ' '))
        out.write(f'[/table]|Gender Identity ({statistics["genderidentityCount"]})[/spoiler]\n')

    with open('circumstancesDeath-spoiler.txt', 'w') as out:
        out.write(f'[spoiler][table]\n')
        for item in statistics['circumstancesDeath']:
            out.write(f'[tr][td]{item}[/td][/tr]\n'.replace('|', ' '))
        out.write(f'[/table]|Circumstances of Death ({statistics["circumstancesDeathCount"]})[/spoiler]\n')

    with open('nickname-spoiler.txt', 'w') as out:
        out.write(f'[spoiler][table]\n')
        for item in statistics['nickname']:
            out.write(f'[tr][td]{item}[/td][/tr]\n'.replace('|', ' '))
        out.write(f'[/table]|Nicknames ({statistics["nicknameCount"]})[/spoiler]\n')


    with open('types-spoiler.txt', 'w') as out:
        for template in statistics['types']:
            values = statistics['types'][template]
            unique_values = set(values)
            # count number of each unique value in values
            counts = {value: values.count(value) for value in unique_values}
            # sort by count
            sorted_counts = sorted(counts.items(), key=lambda item: item[1], reverse=True)
            out.write(f'[spoiler][table]\n')
            for item in sorted_counts:
                out.write(f'[tr][td]{item[0]}[/td][td]{item[1]}[/td][/tr]\n'.replace('|', ' '))
            out.write(f'[/table]| Template Types {template}[/spoiler]\n')

