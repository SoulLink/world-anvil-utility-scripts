import json
import os

from pywaclient.api import BoromirApiClient
from pywaclient.exceptions import UnprocessableDataProvided, AccessForbidden

client = BoromirApiClient(
    'SoulLinkUtilityScript',
    'https://www.worldanvil.com/author/SoulLink', 'v1', os.environ['WA_APPLICATION_KEY'], os.environ['WA_AUTH_TOKEN']
)

base_path = 'articles3'
if not os.path.exists(base_path):
    os.mkdir(base_path)

if __name__ == '__main__':

    with open('article_forbidden.csv', 'w') as forbidden:
        with open('article_failed.csv', 'w') as fail:
            for item in range(224, 266):
                if not os.path.exists(f'{base_path}/{item}'):
                    os.mkdir(f'{base_path}/{item}')
                with open(f'ids/{item}.csv', 'r') as file:
                    for line in file.readlines():
                        line_split = line.strip().split(',')
                        print(
                            f'Processing {line_split[0]} from {line_split[1]}'
                        )
                        try:
                            article = client.article.get(line_split[0], 2)
                            with open(f'{base_path}/{item}/{article["id"]}.json', 'w') as fp:
                                json.dump(article, fp)
                        except UnprocessableDataProvided as e:
                            fail.write(line)
                            continue
                        except AccessForbidden as e:
                            forbidden.write(line)
                            continue



