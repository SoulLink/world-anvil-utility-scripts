import json
import os
from simple_elastic import ElasticIndex

index = ElasticIndex('contest-submissions-sc23-v1', url='localhost:9200')

ignore_properties_in_loop = [
    'id',  # ignored
    'title',
    'slug',  # ignored
    'state',
    'url',  # ignored
    'entityClass',
    'icon',
    'subscribergroups',  # ignored
    'position',  # ignored
    'isWip',
    'isDraft',
    'excerpt',
    'template',
    'customArticleTemplate',
    'wordcount',
    'creationDate',
    'updateDate',
    'publicationDate',
    'notificationDate',
    'likes',
    'views',
    'tags',
    'metadata_user',  # ignored
    'metadata_article',  # ignored
    'css_classes',  # ignored
    'css_styles',  # ignored
    'passcode',  # ignored
    'author',
    'category',
    'world',
    'cover',
    'content',
    'contentParsed',
    'portrait',
    'flag',
    'fullRender'
]

prompt_map = {
    '224': 'A powerful organization in your world',
    '225': 'A seat of power (of any kind!)',
    '226': 'A resource that provides fuel or power',
    '227': 'An animal associated with, or symbolizing, power',
    '228': 'A culture that has suffered under the rule of a stronger nation',
    '229': 'A conflict between two unequal powers in your world',
    '230': 'The title & responsibilities of an important person in your world',
    '231': 'A destructive natural or supernatural event',
    '232': 'A species known for its mischievous personality',
    '233': 'A popular summer tradition that involves art and creativity',
    '234': 'An unclaimed, unregulated, or lawless region in your setting',
    '235': 'A settlement at the limits of the "known" or "civilized" world',
    '236': 'A job that takes its practitioners to remote or faraway places',
    '237': 'An animal found in a non-populated area',
    '238': 'A useful plant found in a wild area of your world',
    '239': 'A material or natural resource that comes from a dangerous location',
    '240': "A character driven by wanderlust or the desire to explore",
    '241': 'A cuisine from a sparse, barren or remote region in your world',
    '242': 'An iconic building or landmark representing a location',
    '243': 'A letter sent in secret by a well-known person in your world',
    '244': 'A tradition or behavior considered old fashioned',
    '245': 'An item of great cultural or religious significance to a people in your world',
    '246': 'An ancient city that is still inhabited today',
    '247': 'A historical figure still venerated today, and why',
    '248': 'A profession that has been rendered obsolete',
    '249': "A children's tale or song based on a real event",
    '250': 'A species now considered extinct',
    '251': 'A historical culture whose influence is still felt today',
    '252': 'A ceremony that represents a transition or transfer',
    '253': 'A rare natural phenomenon that most people look forward to',
    '254': "A method used to carry goods over long distances",
    '255': 'A system to send messages between distant places',
    '256': 'A form of silent communication',
    '257': 'An organization for which recruiting or proselytizing is important',
    '258': 'A species with an unusual form of communication',
    '259': 'A character who excels in manipulating others',
    '260': 'An important public announcement that one person addressed to many',
    '261': 'A building or landmark used for, or associated with, communication',
    '262': 'A character who prefers to lurk in the shadows',
    '263': 'A "negative" condition that has hidden advantages',
    '264': 'An organisation dedicated to keeping a major secret from the public',
    '265': 'A myth or truth about the meaning of your universe',
}


def check_for_data(doc, doc_prop_name, source, source_1, source_2, source_3):
    if source_1 in source and source[source_1] is not None:
        if source_2 in source[source_1] and source[source_1][source_2] is not None:
            if source_3 in source[source_1][source_2] and source[source_1][source_2][source_3] is not None:
                doc[doc_prop_name] = source[source_1][source_2][source_3]


def check_for_data_relations(doc, doc_prop_name, source, source_1, source_2, source_3, source_4):
    if source_1 in source and source[source_1] is not None:
        if source_2 in source[source_1] and source[source_1][source_2] is not None:
            if source_3 in source[source_1][source_2] and source[source_1][source_2][source_3] is not None:
                if source_4 in source[source_1][source_2][source_3] and source[source_1][source_2][source_3][source_4] is not None:
                    doc[doc_prop_name] = source[source_1][source_2][source_3][source_4]


if __name__ == '__main__':
    for directory in os.listdir('articles'):
        print("Processing", directory)
        for file in os.listdir(f'articles/{directory}'):
            # print("Processing ", file)
            path = f'articles/{directory}/{file}'
            with open(path, 'r') as fp:
                article = json.load(fp)
            document = dict()
            document['duel'] = directory
            document['prompt'] = prompt_map[directory]
            document['title'] = article['title']
            document['template'] = article['template']
            document['entityClass'] = article['entityClass']
            if 'customArticleTemplate' in article:
                document['custom_article_template'] = article['customArticleTemplate']['name']
            document['is_wip'] = article['isWip']
            document['is_draft'] = article['isDraft']
            document['state'] = article['state']
            document['icon'] = article['icon']
            document['excerpt'] = article['excerpt']
            document['likes'] = article['likes']
            document['views'] = article['views']
            document['wordcount'] = article['wordcount']
            document['creationDate'] = article['creationDate']['date'].replace(' ', 'T')
            document['updateDate'] = article['updateDate']['date'].replace(' ', 'T')
            document['publicationDate'] = article['publicationDate']['date'].replace(' ', 'T')
            if 'notificationDate' in article and article['notificationDate'] is not None:
                document['notificationDate'] = article['notificationDate']['date'].replace(' ', 'T')
            if 'tags' in article and article['tags'] is not None:
                document['tags'] = article['tags'].split(',')
            document['url'] = article['url']
            if 'category' in article and article['category'] is not None:
                document['category.title'] = article['category']['title']
                document['category.url'] = article['category']['url']
            if 'portrait' in article and article['portrait'] is not None:
                document['portrait'] = True
            else:
                document['portrait'] = False
            if 'flag' in article and article['flag'] is not None:
                document['flag'] = True
            else:
                document['flag'] = False
            document['world.title'] = article['world']['title']
            document['world.url'] = article['world']['url']
            document['author.username'] = article['author']['title']
            document['author.url'] = article['author']['url']
            if 'cover' in article and article['cover'] is not None:
                document['cover'] = True
            else:
                document['cover'] = False
            document['content.vignette'] = article['content']
            for key in article:
                if key not in ignore_properties_in_loop:
                    if key in ['sections']:
                        for subkey in article[key]:
                            document['section.' + subkey] = article[key][subkey]['content']
                    elif key in ['relations']:
                        for subkey in article[key]:
                            if article[key][subkey]['type'] == 'singular':
                                document[f'relation.{subkey}.title'] = article[key][subkey]['items']['title']
                                document[f'relation.{subkey}.type'] = article[key][subkey]['items']['type']
                            elif article[key][subkey]['type'] == 'collection':
                                document[f'relation.{subkey}.title'] = list()
                                document[f'relation.{subkey}.type'] = list()
                                if 'items' in article[key][subkey]:
                                    for c in article[key][subkey]['items']:
                                        document[f'relation.{subkey}.title'].append(c['title'])
                                        document[f'relation.{subkey}.type'].append(c['type'])
                            elif article[key][subkey]['type'] == 'article':
                                document[f'relation.{subkey}.title'] = article[key][subkey]['title']
                            elif article[key][subkey]['type'] == 'timeline':
                                document[f'relation.{subkey}.title'] = article[key][subkey]['title']
                                document[f'relation.{subkey}.type'] = article[key][subkey]['type']
                            else:
                                print("Relation Type:", article[key][subkey]['type'])
                    else:
                        print(key)
            index.index_into(document, article['id'])
