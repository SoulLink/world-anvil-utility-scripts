from os import WIFCONTINUED
import re
import json
import csv

table_items = "<td>(.*?)</td>"
links = '<a href="(.*?)" class="btn btn-xs btn-primary"> <i class="fas fa-eye"></i></a>'


data = list()

with open('site.html', 'r') as fp:
    text = fp.read()

    count = 0
    for match in re.findall(table_items, text):
        if count == 0:
            item = dict()
            item['id'] = match
            count += 1
            data.append(item)
        elif count == 1:
            item['prompt'] = match
            count += 1
        elif count == 2:
            item['published'] = match
            count += 1
        elif count == 3:
            item['template'] = match
            count += 1
        elif count == 4:
            item['note'] = match
            count += 1
        elif count == 5:
            item['video'] = match
            count = 0
    count = 0        
    for match in re.findall(links, text):
        data[count]['link'] = 'https://www.worldanvil.com/' + match
        count += 1

with open('output.json', 'w') as fp:
    json.dump(data, fp, ensure_ascii=False, indent='    ')

with open('output.csv', 'w') as fp:
    writer = csv.writer(fp, dialect='unix')

    for item in data:
        writer.writerow([
            item['id'],
            item['prompt'],
            item['template'],
            item['link']
        ])