import csv
import os
from typing import List

from pywaclient.api import AragornApiClient

if __name__ == '__main__':
    client = AragornApiClient(
        'WorldEmberUtilityScript',
        'https://www.worldanvil.com/author/SoulLink', 'v1', os.environ['WA_APPLICATION_KEY'],
        os.environ['WA_AUTH_TOKEN']
    )

    articles = list()
    with open('input.csv', 'r') as fp:
        reader = csv.reader(fp, dialect='unix')
        count = 0

        for row in reader:
            if count == 0:
                headers: List[str] = row
                count += 1
            else:
                data = dict()
                for index in range(len(row)):
                    content = row[index].strip()
                    if content != '':
                        data[headers[index]] = content
                articles.append(data)

    with open('delete_list.txt', 'w') as fp:
        for article in articles:
            if 'id' in article:
                identifier = article['id']
                del article['id']
                client.article.patch(identifier, article)
            else:
                identifier = client.article.post(article)
                fp.write(identifier + '\n')
